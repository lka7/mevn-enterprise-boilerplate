import { shallowMount } from "@vue/test-utils";
import Intro from "@/components/Intro.vue";

describe("HelloWorld.vue", () => {
  it("renders props.msg when passed", () => {
    const title = "Iinit";
    const wrapper = shallowMount(Intro, {
      propsData: { title }
    });
    expect(wrapper.text()).toMatch(title);
  });
});
